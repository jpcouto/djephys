#!/usr/bin/env python
# Install script for djephys
# Joao Couto - October 2018

import os
from setuptools import setup

longdescription = '''Analysis tools for high density probes with datajoint.'''

setup(
    name = 'djephys',
    version = '0.0',
    author = 'Joao Couto',
    author_email = 'jpcouto@gmail.com',
    description = ('Analysis tools for high density probes with datajoint.'),
    long_description = longdescription,
    license = 'GPL',
    packages = ['djephys'],
#    entry_points = {
#        'console_scripts': [
#            ]
#        }
    )
