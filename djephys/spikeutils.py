import numpy as np

def get_triggered_spikes(sp,onsets,tdur = 1,tpre = 0):
    return [sp[(sp>=on-tpre) & (sp<(on-tpre+tdur))] - on for on in onsets]

def spikesFromRaster(raster,ntrials = None):
    trials,spikes = raster
    if ntrials is None:
        ntrials = np.max(trials)+1
    sp = []
    for iTrial in range(int(ntrials)):
        idx = (trials == iTrial)
        if np.sum(idx):
            sp.append(spikes[idx])
        else:
            sp.append([])
    return sp

def orientation_selectivity(tet,R):
    Lori = lambda R,tet:np.abs(np.sum(R*np.exp(2.*1j*tet))/np.sum(R))
    Ldir = lambda R,tet:np.abs(np.sum(R*np.exp(1.j*tet))/np.sum(R))
    return Lori(R,tet),Ldir(R,tet)

def alpha_function(N, amplitude = 1, t_rise = 2, t_decay = 250, srate = 1000.,norm = True):

    t_rise = t_rise/srate;
    t_decay = t_decay/srate;
    
    fun_max  = (t_rise*t_decay/(t_decay-t_rise)) * np.log(t_decay-t_rise);
    normalization_factor = 1; #%(exp(-fun_max/t_rise) - exp(-fun_max/t_decay))/(t_rise-t_decay);
    ii = np.arange(0,N)
    kernel = np.hstack([np.zeros(N),
                        amplitude*(1.0/(normalization_factor*(t_decay-t_rise))) * (np.exp(-((ii/srate)/t_decay))
                                                                                   - np.exp(-(ii/srate)/t_rise))])
    if norm:
        kernel /= np.sum(kernel)
    return kernel

def binary_spikes(spks,edges,sigma=None,kernel = None):
    ''' Create a vector of binary spikes.

    binsize = 0.001
    edges = np.arange(0,5,binsize)
    bspks = binary_spikes(spks,edges,5)/binsize

    Joao Couto - March 2016
    '''

    bins = [np.histogram(sp,edges)[0] for sp in spks]
    if not sigma is None:
        # Convolve spk trains
        x = np.arange(np.floor(-3*sigma),np.ceil(3*sigma))
        kernel = np.exp(-(x/sigma)**2/2)/(sigma*np.sqrt(2*np.pi))
    if not kernel is None:
        bins = [np.convolve(a,kernel,'same') for a in bins]
    return np.vstack(bins)


def response_frequency_components(spikes,tf,dur,sigma = 5,dt = 0.001):
    # First get the response by convolving with a gaussian kernel
    edges = np.arange(0,dur,dt)
    time = edges[:-1]+np.diff(edges)
    bsp = binary_spikes(spikes,edges,sigma = sigma)/dt
    # Part of the analytical expression for a frequency f (in Hz) 
    F = lambda f: np.exp( -1j * (2. * np.pi * f) * time);
    # Integrate the multiplication of the exp with each point of the estimated
    #response for the stimulus frequency and zero to get f0 and f1.
    f2 = (np.sum(F(tf)*2.*bsp,axis = 1)*dt/dur)*2.0 # Scale f1 and f2 to represent the peak-to-peak
    f1 = (np.sum(F(tf)*bsp,axis = 1)*dt/dur)*2.0
    f0 = (np.sum(F(tf)*bsp,axis = 1)*dt/dur)
    return f0,f1,f2

def compute_threshold(x,stdmin=4):
    ''' Compute threshold from filtered raw trace.

    thresh = compute_spike_threshold(x,stdmin=4)

    Uses the formula from R. Quiroga, Z. Nadasdy, and Y. Ben-Shaul:
       thr = stdmin*sigma_n ,with
       sigma_n = median(|x|/0.6745)
 NOTE:
   Default stdmin is 4.
   In the WaveClus batch scripts stdmin is set to 5.

    Joao Couto - March 2016
    '''
    return stdmin * np.median(np.abs(x))/0.6745;

def find_spikes(dat,thresh = None,wpre=16,wpos=20,threshstd=6,align=True):
    ''' Find spikes and extract sample times and waveforms.
        If align == True, tries to align spikes.

        timestamps = find_spikes(dat)

    Joao Couto - March 2016
    '''
    tmp = np.zeros(shape=dat.shape)
    if thresh is None:
        thresh = compute_spike_threshold(dat,threshstd)
    tmp[dat<-thresh] = 1
    tstamps = np.where(np.diff(tmp)>0)[0]
    tstamps = tstamps[(tstamps>wpre) & (tstamps<(len(data)-wpos))]
    waves = np.array([])
    if not align:
        return tstamps
    # align...
    for i,t in enumerate(tstamps):
        tmp = dat[t-wpre:t+wpos]
        tmpmax = np.argmin(tmp)
        tstamps[i] = t-wpre+tmpmax
    return tstamps


from numpy.lib.stride_tricks import as_strided

def _check_arg(x, xname):
    x = np.asarray(x)
    if x.ndim != 1:
        raise ValueError('%s must be one-dimensional.' % xname)
    return x

def xcorr(x, y, maxlag):
    """
    Cross correlation with a maximum number of lags.

    `x` and `y` must be one-dimensional numpy arrays with the same length.

    This computes the same result as
        numpy.correlate(x, y, mode='full')[len(a)-maxlag-1:len(a)+maxlag]

    The return vaue has length 2*maxlag + 1.

    Adapted from code from Warren Weckesser (stackoverflow).
    
    Joao Couto - January 2016
    """
    x = _check_arg(x, 'x')
    y = _check_arg(y, 'y')
    py = np.pad(y.conj(), 2*maxlag, mode='constant')
    T = as_strided(py[2*maxlag:], shape=(2*maxlag+1, len(y) + 2*maxlag),
                   strides=(-py.strides[0], py.strides[0]))
    px = np.pad(x, maxlag, mode='constant')
    return T.dot(px)
