from glob import glob
import os
import sys
import json
from os.path import join as pjoin

preferencepath = pjoin(os.path.expanduser('~'), 'djephys')

defaultPreferences = {'datajoint':{'database.host':'localhost',
                                   'database.user':'root',
                                   'database.password':'Gcamp99',
                                   'safemode':False,
                                   'reserve_jobs':True,
                                   'display.limit':30},
                      'paths':dict(servers =[
                          '/quadraraid/data',
                          '/mnt/nerffs01/mouselab/data'],
                                   onephoton = '1photon/raw',
                                   twophoton = dict(raw='2photon/raw',
                                                    reg='2photon/reg',
                                                    regds='2photon/reg_ds',
                                                    projections = '2photon/reg_ds'),
                                   ephys = dict(raw = 'ephys/raw',
                                                sort = 'ephys/sorted'),
                                   log = 'presentation',
                                   facecam = 'facecam',
                                   eyecam = 'eyecam',
                                   analysis = 'analysis')}

all = ['get_preferences','preferences',
       'lowpass',
       'runpar']

def get_preferences(user = None):
    ''' 
    Reads the user parameters from the home directory.

    pref = get_preferences(username)

    User parameters like folder location, file preferences, paths...
    Joao Couto - October 2018
    '''
    if not os.path.isdir(preferencepath):
        os.makedirs(preferencepath)
        print('Creating preference folder ['+preferencepath+']')

    if user is None:
        preffile = pjoin(preferencepath,'preferences.json')
    else:
        preffile = pjoin(preferencepath,'{0}.json'.format(user))

    if not os.path.isfile(preffile):
        with open(preffile, 'w') as outfile:
            json.dump(defaultPreferences, outfile, sort_keys = True, indent = 4)
            print('Saving default preferences to: ' + preffile)
    with open(preffile, 'r') as infile:
        pref = json.load(infile)
    return pref


preferences = get_preferences()

def structuredArrayToDict(entries):
    out = {}
    for k,key in enumerate(entries.dtype.names):
        entry = entries[key] 
        if not type(entry) is str:
            try:
                if entry.dtype.char == 'S':
                    entry = entry.astype(str)
            except:
                pass
        out[key] = entry
    return out

##################################################
############## Multiprocessing ###################
##################################################

from multiprocessing import Pool,cpu_count
from functools import partial

def runpar(f,X,nprocesses = None,**kwargs):
    ''' 
    res = runpar(function,          # function to execute
                 data,              # data to be passed to the function
                 nprocesses = None, # defaults to the number of cores on the machine
                 **kwargs)          # additional arguments passed to the function (dictionary)

    '''
    if nprocesses is None:
        nprocesses = cpu_count()
    with Pool(processes=nprocesses) as pool:
        res = pool.map(partial(f,**kwargs),X)
    pool.join()
    return res

##################################################
################ Signal ##########################
##################################################

from scipy.interpolate import interp1d
from scipy.signal import butter,filtfilt

def lowpass(x,Wfraction = 0.3,order = 5):
    ''' 
    filtered = lowpass(x,
                       Wfraction = 0.3, # filter normalized bandwith Wfraction 
                                        #  (filter freq)/(2.* sampling rate)
                        order = 5)      # order of the filter
    
    Low pass filter data or array with a butterworth zero-phase shift filter.
    '''
    B, A = butter(order, Wfraction, btype='low') # nth order Butterworth low-pass
    return filtfilt(B, A, x, axis=0)
