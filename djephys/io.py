import numpy as np
import os
from glob import glob
import pandas as pd
from os.path import join as pjoin


#################################################################
#################### Binary files and spikeglx ##################
#################################################################
def load_dat(fname,nchannels,dtype=np.int16):
    '''
    mmap = load_dat(fname,nchannels,dtype)
    memory-maps a binary file [nsamples x channels].
    '''
    dt = np.dtype(dtype)
    if not os.path.exists(fname):
        raise(ValueError('File '+ fname +' does not exist.'))
    nsamples = os.path.getsize(fname)/(nchannels*dt.itemsize)
    return np.memmap(fname,mode='r',
                     dtype=dt,
                     order = 'C',
                     shape = (int(nsamples),int(nchannels)))


def load_spikeGLX(fname,dtype=np.int16):
    ''' Loads spikeGLX data.
    Joao Couto - July 2015'''
    name = os.path.splitext(fname)[0]
    ext = '.meta'

    metafile = name + ext
    if not os.path.isfile(metafile):
        raise(ValueError('File not found: ' + metafile))
    with open(metafile,'r') as f:
        meta = {}
        for ln in f.readlines():
            tmp = ln.split('=')
            tmp[0] = tmp[0].strip()
            try:
                meta[tmp[0]] = float(tmp[1].replace('\r\n',''))
            except:
                meta[tmp[0]] = tmp[1].replace('\r\n','')
    if not 'nChans' in meta.keys():
        meta['nChans'] = meta['nSavedChans']
        def updateMeta(meta,spkglkey):
            meta['sRateHz'] = meta[spkglkey + 'SampRate']
            meta['rangeMax'] = meta[spkglkey + 'AiRangeMax']
            meta['rangeMin'] = meta[spkglkey +'AiRangeMin']
        # For Imec data (neuropixels)   
        try:
            updateMeta(meta,'ni')
        except:
            updateMeta(meta,'im')
            
        try:
            meta['auxGain'] = meta[spkglkey + 'MNGain']
        except:
            meta['auxGain'] = 1.
    fd = load_dat(fname,meta['nChans'],dtype=dtype)
    return fd,meta

def parse_spikeGLX_digital(dat,channels):
    ''' Extract rise and fall timestamps from digital channel.
        Threshold is auto from channel
    digital_channels = [129,130,131,132,133,134]
    rise,fall = parse_spikeGLX_digital(data,digital_channels)

    Joao Couto - October 2018

    '''
    digdata = dat[:,channels]
    bdigdata = np.zeros_like(digdata,dtype=np.int0)
    thresholds = (np.max(digdata,axis = 0) - np.min(digdata,axis = 0))/2
    rises = []
    falls = []
    for iChan in range(digdata.shape[1]):
        if thresholds[iChan] < 1000:
            print('Found an empty channel [{0} - threshold {1}].'.format(iChan,thresholds[iChan]))
            rises.append([])
            falls.append([])
        bdigdata[digdata[:,iChan]>thresholds[iChan],iChan] = 1
        rises.append(np.nonzero(np.diff(bdigdata[:,iChan])>0)[0])
        falls.append(np.nonzero(np.diff(bdigdata[:,iChan])<0)[0])
    ## Plot all
    # %matplotlib notebook
    # for i,(r,f) in enumerate(zip(rises,falls)):
    #     plt.vlines(r,i+.2,i+.8,color='k')
    #     plt.vlines(f,i+.2,i+.8,color = 'r')
    ## Sanity check
    # %matplotlib notebook
    # plt.plot(digdata[:,-1])
    # plt.vlines(rises[-1],20000,25000)
    return rises,falls

#################################################################
#################### Load Phy data ##############################
#################################################################

def load_phy_timestamps(folder,selection='all',output_cluinfo = False):
    '''
    Get timestamps from phy data

    spks,unit_ids = load_units_ts(folder,selection='good')

    Joao Couto - March 2016
    '''
    ts = np.load(pjoin(folder,'spike_times.npy'))
    clus = np.load(pjoin(folder,'spike_clusters.npy'))
    if selection == 'all':
        unit_ids = np.unique(clus)
    else:
        cluinfo = get_phy_cluster_groups(folder)
        unit_ids = np.array(cluinfo[cluinfo.group == selection].cluster_id)
    spks = [ts[clus==i] for i in unit_ids]
    if not output_cluinfo:
        return spks,unit_ids
    else:
        if selection == 'all':
            cluinfo = get_phy_cluster_groups(folder)
        return spks,unit_ids, cluinfo

def get_phy_cluster_groups(sortfolder, create = False, verbose = False):
    df = None
    sortfolder = os.path.abspath(sortfolder)
    for root, dirs, filenames in os.walk(sortfolder):
        for filename in filenames:
            if not '/.' in root:
                if 'cluster_groups.csv' in filename:
                    folder = root
                    df = pd.read_csv(pjoin(folder,'cluster_groups.csv'),
                                     header=0,
                                     index_col=None,
                                     sep='\t')
                    if verbose:
                        print('Loaded '+ pjoin(folder,'cluster_groups.csv'))
                        
    if df is None and create:
        clus = None
        for root, dirs, filenames in os.walk(sortfolder):
            for filename in filenames:
                if not '/.' in root:
                    if 'spike_templates.npy' in filename:
                        folder = root
                        clus = np.load(pjoin(folder,filename))
                    if 'spike_clusters.npy' in filename:
                        folder = root
                        print(folder)
                        clus = np.load(pjoin(folder,filename))
        assert not clus is None, 'Could not find clu file.'
        unit_id = np.unique(clus)
        unitscladic = {'cluster_id':unit_id,
              'group':np.array(['unsorted' for u in unit_id])}
        df = pd.DataFrame.from_dict(unitscladic)
        df.to_csv(folder+'/cluster_groups.csv',
                                 index=False,
                                 sep='\t')
        print('cluster_groups.csv created in ' + folder)
    return df
