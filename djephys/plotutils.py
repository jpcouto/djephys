import pylab as plt
import numpy as np
from scipy import signal
from .signalutils import filter_data

colors = ['#000000',
          '#d62728',
          '#1f77b4',
          '#ff7f0e',
          '#2ca02c',
          '#9467bd',
          '#8c564b',
          '#e377c2',
          '#7f7f7f',
          '#bcbd22',
          '#17becf']

mplstyledict = {'pdf.fonttype' : 42,
                'ps.fonttype' : 42,
                'axes.titlesize' : 16,
                'axes.labelsize' : 14,
                'axes.facecolor': 'none',
                'axes.linewidth' : 1,
                'axes.spines.right': False,
                'axes.spines.top': False,
                'axes.titleweight': 'bold',
                'axes.prop_cycle': plt.cycler('color', colors),
                'ytick.major.size': 9,
                'xtick.major.size': 9,
                'xtick.labelsize' : 12,
                'ytick.labelsize' : 12,
                'xtick.major.width': 1,
                'ytick.major.width': 1,
                'figure.edgecolor': 'none',
                'figure.facecolor': 'none',
                'figure.frameon': False,
                'font.family': ['sans-serif'],
                'font.size'  : 12,
                'font.sans-serif': ['Helvetica',
                                    'Arial',
                                    'DejaVu Sans',
                                    'Bitstream Vera Sans',
                                    'Computer Modern Sans Serif',
                                    'Lucida Grande',
                                    'Verdana',
                                    'Geneva',
                                    'Lucid',
                                    'Avant Garde',
                                    'sans-serif'],             
                'lines.linewidth' : 1.5,
                'lines.markersize' : 4,
                'image.interpolation': 'none',
                'image.resample': False,
}
plt.matplotlib.style.use(mplstyledict)


def plot_multichannel_data(dat, chorder, srate = 25000.,
                           offset = 1000, filterdata = False,
                           removedc = True, colors=None,scalebar=(0.2,100)):
    ''' Plot raw data ordered by channel '''
    time = np.arange(dat.shape[0])/float(srate)
    ax = plt.gca()
    if colors is None:
        colors = [[0,0,0] for i in range(len(chorder))]
    if filterdata:
        # Low pass signal
        y = filter_data(dat[:,chorder.astype(int)],500,0.95*(srate/2),srate)
    else:
        y = dat[:,chorder.astype(int)]
    if removedc:
        y -= np.mean(y[0:int(0.1*y.shape[0]),],axis=0)
    y += np.arange(y.shape[1])*offset
    plts = ax.plot(time,y,'k',lw=0.6,clip_on=False)

    for i,p in enumerate(plts):
        p.set_color(colors[i])
    for ii,ch in enumerate(chorder):
        ax.text(time[0],y[0,ii],str(int(ch)),color=[.3,.7,.3],
                fontsize=13,fontweight='bold',va='center',ha='right')
    ax.axis([time[0],time[-1],np.min(y),np.max(y)])
    if not scalebar is None:
        x = time[int(0.05*y.shape[0])]+np.array([0,scalebar[0]])
        y = np.min(y) + np.array([0,0])

        ax.plot(x,y,color='k',clip_on=False)
        ax.text(np.diff(x)/2. + x[0],y[0],'{0} ms'.format(1000*scalebar[0]),
                va = 'top', ha='center')
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.set_frame_on(False)

def plot_prb_sites(probe,site_distance=20):
    ''' Plot probe sites and connections.
    Joao Couto - February 2016
    '''
    for ii,chan in enumerate(probe[0]['channels']):
        xy = probe[0]['geometry'][chan]
        plt.plot(xy[0],xy[1],'ko',markersize=20,markerfacecolor='gray',
                 markeredgecolor='black')
        plt.text(xy[0]+site_distance*.05,
                 xy[1]+site_distance*.05,
                 '{0}'.format(chan),
                 color='green')
    for conn in probe[0]['graph']:
        orig = probe[0]['geometry'][conn[0]]
        dest = probe[0]['geometry'][conn[1]]
        plt.plot([orig[0],dest[0]],[orig[1],dest[1]],
                 lw = np.random.uniform(0.8,2.5))

def plot_raster(spks,offset=0.0,height=1.0,colors='black',ax = None):
    ''' Plot a raster from sets of spiketrains.
            - "spks" is a list of spiketrains
            - "colors" can be an array of colors
        Joao Couto - January 2016
    '''
    if ax is None:
        ax = plt.gca()
    nspks = len(spks)
    if type(colors) is str:
        colors = [colors]*nspks
    for i,(sp,cc) in enumerate(zip(spks,colors)):
        ax.vlines(sp,offset+i,
                  offset+i+height,
                  colors=cc,lw=0.5)

def remove_ax_ticks():
    ax = plt.gca()
    ax.get_yaxis().set_tick_params(which='both', direction='out')
    ax.get_xaxis().set_tick_params(which='both', direction='out')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_axis_bgcolor('none')


def plot_footprints(waves, channel_xy,
                    nwaves = 10,
                    gain = (20,10),
                    srate = 25000,
                    plotscale=(1,10),color='k',lw=1.):
    '''Plots multichannel waveforms (shape=(nspikes,nsamples,nchannels)) or (shape=(nsamples,nchannels)).
    '''
    wtime = 1000.*np.arange(waves.shape[0])/float(srate)
    p = []
    ax = plt.gca()
    if len(waves.shape) < 3:
        # plotting mean waveforms
        for i in range(waves.shape[1]):
            p.append(ax.plot(wtime*gain[0] + channel_xy[i,0],
                              waves[:,i]*gain[1]+channel_xy[i,1],color=color,lw=lw))
        miny = np.min(abs(np.diff(channel_xy[:,1])))
        ax.axis([np.min(channel_xy[:,0]),
                 np.max(channel_xy[:,0])+wtime[-1]*gain[0],
                np.min(channel_xy[:,1])-miny,
                np.max(channel_xy[:,1])+miny])
        ax.axis('off')
    if not plotscale is None:
        t = []
        # X scale
        x = plotscale[0] * np.array([0,1])*gain[0] - gain[0]/6.
        y = np.array([0,0])+np.min(channel_xy[-1,0]) - gain[1]/2.

        p.append(plt.plot(x,y,'k',clip_on = False,lw = 1))
        t.append(ax.text(np.diff(x)/2. + np.min(x),np.min(y),
                r'{0} ms'.format(plotscale[0]),
                va = 'top',ha='center',fontsize=8))
        # Y scale
        x = np.array([0,0])-gain[0]/6.
        y = np.array([0,plotscale[1]*gain[1]])+np.min(channel_xy[-1]) - gain[1]/2.
        p.append(plt.plot(x,y,'k',clip_on = False,lw = 1))
        t.append(ax.text(np.min(x),np.diff(y)/2. + np.min(y),
                r'{0} $\mu$V'.format(plotscale[1]),
                rotation = 90,va = 'center',ha='right',fontsize=8))
        return p,t
    return p

def plot_orientation_polar(orinames,f1):
    x = np.radians(orinames[:-1])
    y = np.mean(f1[:-1,:],axis = 1)
    ystde = np.std(f1[:-1,:],axis = 1)/np.sqrt(f1.shape[1])
    x = np.append(x,x[0])
    y = np.append(y,y[0])
    ystde = np.append(ystde,ystde[0])
    blank = np.mean(f1[-1,:])
    ###

    plt.fill_between(x,y-ystde, y+ystde,facecolor=np.ones((3,))*0.5,edgecolor='none',alpha = 0.8)
    plt.plot(x,np.zeros(x.shape) + blank,color='lightblue',linestyle = '-')
    plt.plot(x,y,'k')
    plt.gca().set_thetagrids(np.degrees(x), frac=1.2)
    try:
        if blank>0:
            plt.gca().set_rgrids(np.sort([np.ceil(np.max(blank)*10)/10.,np.ceil(np.max(y)*10)/10.]))
        else:
            plt.gca().set_rgrids([np.ceil(np.max(y)*10)/10.])
    except:
        print('Could not set polar gridlines...')
        
    gridlines = plt.gca().get_xgridlines() + plt.gca().get_ygridlines()
    for line in gridlines:
        line.set_linestyle('-')
        line.set_linewidth(0.5)
        line.set_color('k')
    ticklabels = plt.gca().get_yticklabels()
    for lab in plt.gca().get_xticklabels():
        lab.set_fontsize(8)
    for lab in plt.gca().get_yticklabels():
        lab.set_fontsize(8)
        lab.set_fontweight('bold')
    
def plot_cumulative(X,bins = int(100),**kwargs):
    '''
    plotCumulative(X,bins = int(100),**kwargs)

    Plots the cumulative from a distribution of points.
    Returns : edges,cumulative, plothandle
    '''
    if type(bins) in [int,float]:
        bins = np.linspace(np.min(X),np.max(X),bins)
    cum,ed = np.histogram(np.array(X),bins = bins)
    # cumulatives go til the end of the bin
    a = plt.plot(ed[1:],np.cumsum(cum)/np.sum(cum),**kwargs)
    return ed,cum,a[0]



def plot_histogram_as_line(edges,counts,**kwargs):
    '''
    plotHistogramAsLine(edges,counts,**kwargs)

    Plots an histogram as a line. 
    Arguments are the edges and counts straight out of np.histogram.

    Returns : nx,ny, plothandle
    '''
    
    nx = np.hstack([edges[:-1],edges[1:]])
    nx = np.sort(nx)
    ny = np.zeros_like(nx)
    ny[1::2] = counts[:]
    ny[0::2] = counts[:]
    a = plt.plot(nx,ny,**kwargs)
    return nx,ny,a[0]

def plot_scatter_multiple_variables(data,
                                    cliplimit = [],
                                    color = None,
                                    labels = None,
                                    spread = 0.3,
                                    markerpar = dict(clip_on = False,
                                                     markersize=2,
                                                     alpha = 0.5,
                                                     lw = 0,
                                                     markeredgecolor = 'none')):
    '''
    Plots a random scatter plot of multiple variables (randomizes x values).
        - data is a list of y values
    '''
    if not len(cliplimit):
        cliplimit = [np.min([np.nanmin(d) for d in data]),
                     np.max([np.nanmax(d) for d in data])]
    plts = []
    if color is None:
        color = colors*2
    if len(color) == 1:
        color = color*len(data)
    for i,d in enumerate(data):
        x = np.zeros(len(d)) + i + np.random.uniform(-1.*spread,spread,len(d))
        plts.append(plt.plot(x,np.clip(d,*cliplimit),'.',color=color[i],**markerpar))
    if labels is None:
        labels = [str(i) for i in np.arange(len(data))]
    plt.xticks(np.arange(len(data)),labels)
    plt.axis([-.5,len(data)-1 + .5, cliplimit[0],cliplimit[1]])
    return plts
