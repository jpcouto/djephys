from os.path import join as pjoin
import os
import numpy as np
from decimal import Decimal
import datajoint as dj

from .utils import *

for k in preferences['datajoint'].keys():
    dj.config[k] = preferences['datajoint'][k]

#######################################################    
dj.conn()
expschema = dj.schema('djephys', locals())
#######################################################

@expschema
class MouseStrain(dj.Lookup):
    definition = """
    strain: varchar(50) 
    """
    contents = zip(['C57BL/6J',
                    'Thy1-GCaMP6s',
                    'Rorb-IRES2-Cre-D',
                    'Ai93D.R26-ZtTA',
                    'Ai93D;CaMK2a-tTA',
                    'Rorb-Ai93DCaMK2a-tTA',
                    'PVCre',
                    'Emx1Cre-Ai93D.CaMK2a-tTA',
                    'CaMKII-tTA-TRE-GCaMP6s',
                    'unknown'])
    
@expschema
class Experimenter(dj.Lookup):
    definition = """
    user_id: varchar(2)
    ------
    name: varchar(20)
    mail: varchar(40)
    """
    contents = [
        ['JC','Joao Couto','joao.couto@nerf.be'],
    ]

@expschema
class BrainLocation(dj.Lookup):
    definition = """
    recording_site: varchar(16)       #  
    """
    contents = zip(['V1',
                    'S1',
                    'dLGN',
                    'AL',
                    'RL',
                    'AM',
                    'LI'])

@expschema
class Mouse(dj.Manual):
    ''' Unique descriptor of the experiment subject.
    
    Usage:
        m = dict(mouse_id = "JC001",
                 dob = "1985-02-26",
                 gender = "M",
                 strain = "C57BL/6J")
        Mouse.insert([m])
    '''
    
    definition = """
    mouse_id  : varchar(20)          # unique mouse id
    ---
    dob       : date                 # mouse date of birth
    gender    : enum('M', 'F', 'U')  # sex of mouse - Male, Female, or Unknown
    -> MouseStrain
    """

@expschema
class Session(dj.Manual):
    ''' 
Unique descriptor of an experiment session. 
    Experiment sessions have multiple runs.
    Session() and SessionRuns() must be inserted "manually".
    
    ses = dict(session_date = '2018-10-15',
               session_name = '20181015_JC001_ephys_JC',
               user_id = 'JC'
               experiment_type = 'ephys',
               experiment_setup = 1,
               session_notes = 'Important stuff from the experiment.')
    Session.insert([ses])
    '''
    
    definition = """
    -> Mouse
    session_name       : varchar(64)     # Name of the data folder
    ---
    session_date       : date            # session date
    -> Experimenter
    experiment_type    : enum('behavior','ephys','other')
    experiment_setup   : int             # experiment setup ID
    session_notes = '' : varchar(4000)   # free-text notes
    """

@expschema 
class SessionRun(dj.Manual):
    '''
    Session runs/stimuli in the session.

    runs = [dict(session_name = '181015_JC001_ephys_JC',
                 run_num = 0,
                 session_subname = 'run00_stimulus_descriptor')]
    SessionRun.insert(runs)
    '''
    definition = """
    -> Session
    run_num               : smallint      # number of the run in the session
    session_subname       : varchar(64)   # folder name for the session run
    ---
    session_subnotes = '' : varchar(4000) # free-text notes
    """

@expschema
class EphysRun(dj.Manual):
    '''
    Electrophysiology run (single probe), sync pulses.
    '''
    definition = """
    # Electrophysiology run
    -> SessionRun
    ---
    file_name              : varchar(128)     # filename (raw)
    srate                  : float            # sampling rate (Hz)
    channelmap             : longblob         # Channelmap for this experiment (Probe,Data,Disconnect,X,Y,Shank)
    syncchannels           : blob             # Sync channels 
    syncnames              : varchar(512)     # Sync channels names
    probe = ''             : varchar(64)
    """ 
        
################# Derived tables ######################
# With the write folder structure the remaining of the
# data can be infered from the tables above. 
#######################################################    

@expschema
class EphysSync(dj.Imported):
    
    definition = """
    # Sync signals for each run/channel
    -> EphysRun
    syncname           : varchar(24)    # name of the sync channel 
    ---
    sync_rises         : longblob       # sync channel rise times 
    sync_falls         : longblob       # sync channel fall times
    """
    def _make_tuples(self,key):
        k = (EphysRun() & key).fetch()[0]
        from .pathutils import search_servers
        filename = search_servers(pjoin(preferences['paths']['ephys']['raw'],k['session_name']),'',k['file_name'])
        assert len(filename), 'Raw spikeGLX file not found {0} {1}.'.format(pjoin(preferences['paths']['ephys']['raw'],k['session_name']),k['file_name'])
        datafile = filename[0]
        
        from .io import load_spikeGLX,parse_spikeGLX_digital
        mmap,meta = load_spikeGLX(datafile)
        digchannels = k['syncchannels']
        channames = k['syncnames'].split(';')
        rises,falls = parse_spikeGLX_digital(mmap,digchannels)
        del mmap
        keys = []
        for channame,(rise,fall) in zip(channames,zip(rises,falls)):
            keys.append(dict(key,
                             syncname = channame,
                             sync_rises = rise,
                             sync_falls = fall))
        self.insert(keys)

@expschema
class Sorting(dj.Imported):
    definition = '''
    # Spike sorting for the session
    -> Session
    sort_session      : smallint     # number of the sorting session
    ---
    num_clusters      : int          # number of clusters
    num_units         : int          # number of good clusters
    algorithm = ''    : varchar(16)  # kilosort... 
'''
    class Units(dj.Part):
        definition = '''
        -> Sorting
        -> EphysRun
        unit_id         : int
        ---
        sorting_unit_id : int
        selection       : varchar(16) # good; mua; noise
        timestamps      : longblob
'''
    def _make_tuples(self,key):
        from djephys import load_phy_timestamps
        import numpy as np
        from glob import glob
        from os.path import join as pjoin
        from json import load as jsonload
        from .pathutils import search_servers
        sortses = [a for a in search_servers(pjoin(preferences['paths']['ephys']['sort'],
                                                   key['session_name']),'*','*',
                                             verbose = False) if os.path.isdir(a)]
        assert len(sortses), 'No spike sorting found for {0}'.format(
            pjoin(preferences['paths']['ephys']['sort'],
                  key['session_name']))
        sortkeys = []
        for iSort,phyfolder in enumerate(sortses):
            spks_samples,unit_ids,cluinfo = load_phy_timestamps(phyfolder,
                                                                selection='all',
                                                                output_cluinfo = True)
            sortinfofile = glob(pjoin(phyfolder,'*.json'))[0]
            # Get the offsets from the info file (this is generated when concatenating runs).
            with open(sortinfofile,'r') as s:
                sortinfo = jsonload(s)
            offsets = sortinfo['data_description']['file_offsets']
            print(offsets)
            # Get EphysRuns
            ephysruns = (EphysRun() & key).proj().fetch(as_dict = True)
            unitkeys = []
            
            sortkeys.append(dict(key,sort_session = iSort,
                                 num_clusters = len(spks_samples),
                                 num_units = len(cluinfo[cluinfo.group == 'good']),
                                 algorithm = 'kilosort'))
            print(sortkeys)
            for iSes in range(len(ephysruns)):
                runkey = [a for a in ephysruns if a['run_num'] == iSes][0]
                tstart,tstop = offsets[iSes:iSes+2]
                # use the offsets to separate sessions
                for iunit ,(sp,uid) in enumerate(zip(spks_samples,unit_ids)):
                    unitkeys.append(dict(runkey,
                                         sort_session = iSort,
                                         unit_id = iunit,
                                         sorting_unit_id = uid,
                                         selection = cluinfo[cluinfo.cluster_id == uid].group.values[0],
                                         timestamps = sp[(sp>=tstart) & (sp<tstop)] - tstart))
            self.insert1(sortkeys[-1])
            Sorting.Units().insert(unitkeys)
@expschema
class WaveformStats(dj.Computed):
    definition = """
    # Waveform stats for each unit
    -> EphysRun
    -> Sorting.Units
    ---
    waveforms_sample            : longblob       # Sample waveforms for this unit
    waveforms_mean              : longblob
    data_channel                : int           # principal electrode for this unit (Probe,Data,Disconnect,X,Y,Shank)
    probe_channel               : int
    depth                       : float
    xy                          : blob
    """

    def _make_tuples(self,key):
        k = (EphysRun() & key).fetch()[0]
        from .pathutils import search_servers
        filename = search_servers(pjoin(preferences['paths']['ephys']['raw'],k['session_name']),'',k['file_name'])
        assert len(filename), 'Raw spikeGLX file not found {0} {1}.'.format(pjoin(preferences['paths']['ephys']['raw'],k['session_name']),k['file_name'])
        datafile = filename[0]
        spks,uid = (Sorting.Units() & key).fetch('timestamps','unit_id')
        channelmap = k['channelmap']
        channelmap = channelmap[channelmap[:,2] == 0,:]
        idx = np.argsort(channelmap[:,0])
        channelmap = channelmap[idx,:]
        channelsid = channelmap[:,1]
        from .io import load_spikeGLX,parse_spikeGLX_digital
        mmap,meta = load_spikeGLX(datafile)
        if not len(spks[0]):
            print('Unit [{0} {1} --> {2}] does not have spikes for this stimuli.'.format(k['session_name'],
                                                                                         k['session_subname'],
                                                                                         uid[0]))
            return 
        from .waveformsutils import get_waveforms
        waves = get_waveforms(mmap,channelsid, spks[0],nwaves = 80)
        mwaves = np.nanmean(waves,axis = 0)
        pelec = np.argmax(np.max(np.abs(mwaves[:,:]),axis=1))
        kk  = dict(key,
                   waveforms_sample = waves,
                   waveforms_mean = mwaves,
                   data_channel = channelmap[pelec,1],
                   xy = channelmap[pelec,[3,4]],
                   depth = channelmap[pelec,4],
                   probe_channel = channelmap[pelec,0])
        self.insert1(kk)
    #
    #waves = get_waveforms(dat,np.arange(nchannels), spks,nwaves = nwaves)
    #elec = h[w == u]
    #supra = np.hstack([pelec,elec])
    #supra = supra[np.abs(supra-pelec)<10]
    
        
@expschema
class VisualStimuli(dj.Computed):
    wpre = 10
    wpost = 30
    definition = """
    # Visual stimuli 
    -> SessionRun
    -> EphysRun
    ---
    stim_times            : longblob       # (code,trial,onset,offset)
    stim_duration         : longblob       # (s)
    n_trials              : smallint       # Number of trials
    n_stims               : smallint       # Number of stimuli
    stim_definition       : blob           # load as json 
    stim_options          : blob 
    """
    class Stimulus(dj.Part):
        definition = """
        -> VisualStimuli
        stim_i                 : smallint       # stimulus number
        ---
        stim_onsets            : blob           # onset of each trial
        stim_offsets           : blob           # offset of each trial
        stim_dur               : float          # duration of the stimulus
        stim_parameters_header : varchar(1024)  # header of stimulus parameters (;separated)
        stim_parameters        : varchar(1024)  # stimulus parameters (;separated)
        """
    
    class Rasters(dj.Part):
        definition = """
        -> VisualStimuli.Stimulus
        -> Sorting.Units
        ---
        raster               : longblob
        """
        
    def _make_tuples(self, key):
        # Don't run this if the stimuli is not visual:
        if not ('closedLoopActuator' in key['session_subname']
                or 'gray' in key['session_subname']
                or 'dark' in key['session_subname']
                or 'low_light' in key['session_subname']):
            from .pathutils import search_servers
            filename = search_servers(pjoin(preferences['paths']['log'],
                                           key['session_name']),
                                     key['session_subname']+'.log','')
            if not len(filename):
                print('Logfile not found {0}'.format(key['session_name']))
            else:
                filename = filename[0]
                print('Adding data from log: {0}'.format(filename))
                from pyvstim import parseVStimLog, getStimuliTimesFromLog
                log,comms = parseVStimLog(filename)
                stimtimes,stimpars,stimoptions = getStimuliTimesFromLog(filename,
                                                                        log = log)
                rises,falls = (EphysSync() & key & 'syncname = "screen"').fetch('sync_rises',
                                                                                'sync_falls')
                srate = (EphysRun() & key).fetch('srate')[0]

                stimtimes[:,2] = rises[0].astype(np.float)/srate
                stimtimes[:,3] = falls[0].astype(np.float)/srate
                
                tdur = np.diff(stimtimes[:,np.array([2,3])])
                tdur = np.mean(tdur)
                nstims = len(np.unique(stimtimes[:,0]))
                ntrials = len(np.unique(stimtimes[:,1]))
                nk = dict(key,
                          stim_times = stimtimes,
                          stim_duration = tdur,
                          n_trials = ntrials,
                          n_stims = nstims,
                          stim_definition = stimpars.to_json(),
                          stim_options = stimoptions)
                nk_stimulus = []
                nk_rasters = []
                # get the units and the sampling rate.
                unitid,sort_session,timestamps = (Sorting.Units() & key).fetch(
                    'unit_id','sort_session','timestamps')
                timestamps = [t.astype(np.float)/srate for t in timestamps]
                for iStim,uStim in enumerate(np.unique(stimtimes[:,0])):
                    _,_,onsets,offsets = stimtimes[stimtimes[:,0] == uStim].T
                    tmp = stimpars.iloc[iStim]
                    nk_stimulus.append(dict(key,
                                            stim_i = iStim,
                                            stim_onsets = onsets,
                                            stim_offsets = offsets,
                                            stim_dur = tdur,
                                            stim_parameters_header = ';'.join(
                                                list(tmp.keys())),
                                            stim_parameters = ';'.join([
                                                str(i) for i in tmp.values])))
                    
                    for uid,sortses,sp in zip(unitid,sort_session,timestamps):
                        rast = [np.vstack([itrial + np.zeros(np.sum((sp>=on) &
                                                                    (sp<(on+tdur)))),
                                           sp[(sp>=on) &
                                   (sp<(on+tdur))] - on])
                                for itrial,on in enumerate(onsets)]
                        nk_rasters.append(dict(key,
                                               stim_i = iStim,
                                               unit_id = uid,
                                               sort_session = sortses,
                                               raster = np.hstack(rast)))
                VisualStimuli().insert([nk])
                VisualStimuli.Stimulus().insert(nk_stimulus)
                VisualStimuli.Rasters().insert(nk_rasters)
@expschema
class Responses(dj.Computed):
    definition = """
    -> VisualStimuli
    -> Sorting.Units
    ---
    firing_rate       : longblob
    mean_firing_rate  : longblob
    f0 = NULL         : longblob
    f1 = NULL         : longblob          
    mean_f0 = NULL    : blob
    mean_f1 = NULL    : blob
    """
    def _make_tuples(self,key):
        k = (VisualStimuli() & key).fetch()
        
        nstims = k['n_stims'][0]
        ntrials = k['n_trials'][0]
        stimdur = k['stim_duration'][0]
        from pandas import read_json
        stimpars = read_json(k['stim_definition'][0][0]).sort_index()
        fixed_tf = None
        if 'drift' in stimpars.keys(): # this is the TF
            tfs = stimpars.drift.unique()
            if len(tfs) < 3:
                #print('Using a fixed TF of {0}'.format(stimpars.drift.mode()[0]))
                fixed_tf = stimpars.drift.mode()[0]
        fr = np.zeros((nstims,ntrials))
        f0s = np.zeros((nstims,ntrials))
        f1s = np.zeros((nstims,ntrials))

        for ipar in range(nstims):
            tmp = (VisualStimuli.Rasters()*VisualStimuli.Stimulus() & key & 'stim_i = {0}'.format(ipar)).fetch()
            from .spikeutils import spikesFromRaster, response_frequency_components
            sp = spikesFromRaster(tmp['raster'][0],ntrials)
            stimdict = {}
            for idict,ival in zip(tmp['stim_parameters_header'][0].split(';'),
                                  [t for t in tmp['stim_parameters'][0].split(';')]):
                try:
                    ival = float(ival)
                except ValueError:
                    pass
                    # print("Skipping {0}".format(idict))
                stimdict[idict] = ival

            if 'drift' in stimdict.keys(): # this is the TF
                tf = stimdict['drift']
                if not fixed_tf is None:
                    tf = fixed_tf
                f0,f1,f2 = response_frequency_components(sp,tf,stimdur)
                f0s[ipar,:] = np.abs(f0)
                f1s[ipar,:] = np.abs(f1)
            fr[ipar,:] = np.array([len(s)/stimdur for s in sp])
        self.insert1(dict(key,
                          firing_rate = fr,
                          mean_firing_rate = np.mean(fr,axis = 1),
                          f0 = f0s,
                          f1 = f1s,
                          mean_f0 = np.mean(f0s,axis = 1),
                          mean_f1 = np.mean(f1s,axis = 1)))
        

    def get_rasters(self,key):
        tmp = (VisualStimuli.Rasters() & key).fetch()
        
@expschema
class OrientationStimuli(dj.Computed):
    definition = """
    -> Responses
    ---
    is_responsive = 0 : smallint
    angles : blob
    anova_frs : float
    anova_f0s : float
    anova_f1s : float
    ori_sel = NULL: float
    dir_sel = NULL : float
    ori_pref = NULL: float
    dir_pref = NULL : float
    """
    def _make_tuples(self,key):
        stimname = (EphysRun() & key).fetch('session_subname')
        if not 'direction' in stimname[0]:
            return
        print(key)
        dat = (Responses() & key).fetch()[0]
        if not len(dat):
            print(key)
            return
        from scipy import stats
        
        anova_frs = stats.f_oneway(*np.stack(dat['firing_rate'])).pvalue
        if np.isnan(anova_frs):
            anova_frs = 1
            anova_f0s = 1
            anova_f1s = 1
        else:
            anova_f0s = stats.f_oneway(*np.stack(dat['f0'])).pvalue
            anova_f1s = stats.f_oneway(*np.stack(dat['f1'])).pvalue
        k = (VisualStimuli() & key).fetch()
        nstims = k['n_stims'][0]
        angles = np.zeros(nstims)
        for ipar in range(nstims):
            tmp = (VisualStimuli.Stimulus() & key & 'stim_i = {0}'.format(ipar)).fetch()
            stimdict = {}
            for idict,ival in zip(tmp['stim_parameters_header'][0].split(';'),
                                  [float(t) for t in tmp['stim_parameters'][0].split(';')]):
                stimdict[idict] = ival
            angles[ipar] =  stimdict['ori']
        self.insert1(dict(key,
                          is_responsive = int((anova_frs < 0.01) & (anova_f1s < 0.01)),
                          anova_frs = anova_frs,
                          anova_f0s = anova_f0s,
                          anova_f1s = anova_f1s,
                          angles = angles))
    
@expschema
class EyeTracking(dj.Imported):
    definition = """
    -> EphysRun
    ---
    diameter    : longblob
    time        : longblob
    """

    def _make_tuples(self,key):
        searchpath = pjoin(preferences['paths']['eyecam'],key['session_name'],key['session_subname'])
        from .pathutils import search_servers
        camlogname = search_servers(searchpath,'camlog')
        mptrackerfile = search_servers(searchpath,'mptracker','results.')
        if not len(mptrackerfile[0]):
            print("Could not find results.mptracker [{0}]".format(searchpath))
        assert len(camlogname),"Could not find camlog [{0}]".format(searchpath)
        assert len(mptrackerfile),"Could not find results.mptracker [{0}]".format(searchpath)
        from labcams import parseCamLog
        camlog,camcomm = parseCamLog(camlogname[0])

        frameids = camlog['frame_id']
        from h5py import File as h5file
        with h5file(mptrackerfile[0],'r') as fd:
            diam = fd['diameter'][:]
        srate = (EphysRun() & key).fetch('srate')[0]
        camtime =  (EphysSync() & 'syncname = "cam2"' & key).fetch('sync_rises')[0]/srate
        if not len(camtime) == len(diam):
            print("Camera sync is not the same length as diameter recording? [{0},{1}]".format(
            key['session_name'],key['session_subname']))
            return
        self.insert1(dict(key,
                          diameter = diam,
                          time = camtime))
