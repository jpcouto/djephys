from .signalutils import filter_data
import numpy as np

def get_waveforms(data, datchannels, timestamps, nwaves = 100,
                  daqconv = lambda x: x,
                  npre = 15,npost=25,filterwaveforms = True,
                  sampling_rate=25000.):

    spks2extract = np.random.choice(timestamps,
                          np.clip(nwaves,
                                  1,len(timestamps)),
                          replace=False)
    indexes = np.arange(-npre,npost,dtype=np.int32)
    waveforms = np.zeros((len(spks2extract),len(datchannels),npre+npost),
                         dtype=np.float32)
    mask = lambda x: x
    if filterwaveforms:
        mask = lambda x: filter_data(x,flow = 500, fhigh = 6000,srate=sampling_rate)
    for i,s in enumerate(np.sort(spks2extract)):
        waveforms[i,:,:] = mask(np.take(data[(indexes+s),:].astype(np.float32),datchannels,axis=1).T)
    return daqconv(waveforms)


