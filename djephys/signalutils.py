import numpy as np
import scipy.signal as signal
import scipy.interpolate as interp
from matplotlib import mlab

def filter_data(dat,flow=500,
                fhigh = None,
                srate = 25000,
                butter_order = 3,
                axis = 1):
    '''
    fdat = filter_data(dat, flow, fhigh, srate)
    '''
    if fhigh is None:
        # Highpass signal
        btype = 'highpass'
        b, a = signal.butter(butter_order,flow/(srate/2.), btype = btype)
    elif flow is None:
        btype = 'lowpass'
        # Lowpass signal
        b, a = signal.butter(butter_order,fhigh/(srate/2.), btype = btype)
    else:
        btype = 'bandpass'
        # Bandpass signal
        b, a = signal.butter(butter_order,[i/(srate/2.) for i in [flow,fhigh]], btype = btype)
    return signal.filtfilt(b, a, dat, axis = axis)


def coherence(dd,srate = 25000,nsrate=500,flow=15,fhigh=150):

    fdd = filter_data(dd,flow=flow,fhigh = fhigh)
    nsrate = 500
    fdd = fdd.take(np.arange(0,dd.shape[0],srate/nsrate,dtype=int),axis=0)
    N = fdd.shape[1]
    coh = np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            [tmp,fcoh] = mlab.cohere(fdd[:,i],fdd[:,j],nsrate)
            coh[i,j] = np.mean(tmp)
    return coh
