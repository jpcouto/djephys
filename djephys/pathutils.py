from glob import glob
from os.path import join as pjoin
import os
import numpy as np
from .utils import preferences

def search_servers(datapath, extension,wildchar = '*.',verbose = False):
    '''
    Search files in data servers (specified in preferences)

    files = search_servers(datapath,extension,wildchar = '*.')

    Joao Couto - October 2018
    '''
    for server in preferences['paths']['servers']:
        path = pjoin(server,datapath)
        if verbose:
            print('\t\t' + path)
        if len(extension + wildchar):
            path = pjoin(path,wildchar + extension)
        files = np.sort(glob(path))
        if len(files):
            return files
    return []
